// SPDX-License-Identifier: GPL-2.0
#include <linux/init.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/list.h>
#include <linux/ktime.h>
#include <linux/slab.h>


MODULE_AUTHOR("IO-11 Kochetov Denys");
MODULE_DESCRIPTION("Hello, world in Linux Kernel Training");
MODULE_LICENSE("Dual BSD/GPL");

struct struct_data {
	struct list_head head;
	ktime_t time;
};

static struct list_head my_list_head = LIST_HEAD_INIT(my_list_head);

static uint cycle_param = 1;
module_param(cycle_param, uint, 0444);	// 0444=S_IRUGO
MODULE_PARM_DESC(cycle_param, "'Hello, world!' cycle parameter");


static int __init hello_init(void)
{

	if (cycle_param > 10) {
		pr_err("'cycle_param' is more than 10. ERROR!\n");
		return -EINVAL;
	}

	if (cycle_param == 0 || (cycle_param >= 5 && cycle_param <= 10))
		pr_warn("'cycle_param' equals 0 or is between 5 and 10\n");

	uint counter = 0;

	while (counter != cycle_param) {

		struct struct_data *ptr = kmalloc(sizeof(*ptr), GFP_KERNEL);

		ptr->time = ktime_get();
		list_add_tail(&ptr->head, &my_list_head);

		pr_emerg("Hello world!\n");
		counter++;
	}

	return 0;
}

static void __exit hello_exit(void)
{
	struct struct_data *ptr, *tmp;

	list_for_each_entry_safe(ptr, tmp, &my_list_head, head) {
		pr_info("time = %lld\n", ktime_to_ns(ptr->time));
		list_del(&ptr->head);
		kfree(ptr);
	}
}

module_init(hello_init);
module_exit(hello_exit);
