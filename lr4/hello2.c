// SPDX-License-Identifier: GPL-2.0
#include <hello1.h>

MODULE_AUTHOR("IO-11 Kochetov Denys");
MODULE_DESCRIPTION("Lr №4 hello2.ko module");
MODULE_LICENSE("Dual BSD/GPL");


static uint cycle_param = 1;
module_param(cycle_param, uint, 0444);
MODULE_PARM_DESC(cycle_param, "'Hello, world!' cycle parameter");


static int __init hello2_init(void)
{
	pr_info("hello2.ko init\n");
	print_hello(cycle_param);
	return 0;
}

static void __exit hello2_exit(void)
{
	pr_info("hello2.ko exit\n");
}

module_init(hello2_init);
module_exit(hello2_exit);
