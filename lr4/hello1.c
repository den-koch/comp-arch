// SPDX-License-Identifier: GPL-2.0
#include <hello1.h>

MODULE_AUTHOR("IO-11 Kochetov Denys");
MODULE_DESCRIPTION("Lr №4 hello1.ko module");
MODULE_LICENSE("Dual BSD/GPL");


struct struct_data {
	struct list_head head;
	ktime_t time_before;
	ktime_t time_after;
};

static struct list_head my_list_head = LIST_HEAD_INIT(my_list_head);


int print_hello(uint cycle_param)
{
	if (cycle_param > 10) {
		pr_err("'cycle_param' is more than 10. ERROR!\n");
		return -EINVAL;
	}

	if (cycle_param == 0 || (cycle_param >= 5 && cycle_param <= 10))
		pr_warn("'cycle_param' equals 0 or is between 5 and 10\n");

	uint counter = 0;

	while (counter != cycle_param) {

		struct struct_data *ptr = kmalloc(sizeof(*ptr), GFP_KERNEL);

		ptr->time_before = ktime_get();
		pr_emerg("Hello world!\n");
		ptr->time_after = ktime_get();

		list_add_tail(&ptr->head, &my_list_head);

		counter++;
	}

	return 0;
}
EXPORT_SYMBOL(print_hello);

static int __init hello1_init(void)
{
	pr_info("hello1.ko init\n");
	return 0;
}

static void __exit hello1_exit(void)
{
	struct struct_data *ptr, *tmp;

	list_for_each_entry_safe(ptr, tmp, &my_list_head, head) {
		pr_info("time = %lld\n", ktime_to_ns(ptr->time_after-ptr->time_before));
		list_del(&ptr->head);
		kfree(ptr);
	}
	pr_info("hello1.ko exit\n");
}

module_init(hello1_init);
module_exit(hello1_exit);
